require "blue_hawaii/version"
require "blue_hawaii/rate_calculator"
require "blue_hawaii/rental"
require "blue_hawaii/rental_parser"
require "blue_hawaii/vacation_cost_calculator"

module BlueHawaii
  def self.calculate_cost(rental_json_path, booking_text_file_path)
    vacation_rentals = RentalParser.new(File.read(rental_json_path)).parse
    booking_dates = File.read(booking_text_file_path)
    BlueHawaii::VacationCostCalculator.new(vacation_rentals, booking_dates).prices
  end
end
