require 'date'

module BlueHawaii
  class VacationCostCalculator
    attr_reader :vacation_rentals, :date_range

    DATE_REGEX = /\d{4}\/\d{2}\/\d{2}/

    def initialize(vacation_rentals, date_range)
      @vacation_rentals = vacation_rentals
      @date_range = date_range
    end

    def prices
      @prices ||= vacation_rentals.map { |rental| "#{rental.name}: $#{rental.cost_for_period(start_date, end_date)}" }
    end

    def start_date
      @start_date ||= Date.parse(date_range.match(/^#{DATE_REGEX}/).to_s)
    end

    def end_date
      @end_date ||= Date.parse(date_range.match(/#{DATE_REGEX}$/).to_s) - 1 #rates are nightly
    end
  end
end
