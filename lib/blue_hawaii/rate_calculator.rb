module BlueHawaii
  class RateCalculator
    attr_reader :rental

    def initialize(rental)
      @rental = rental
    end

    def rates_with_range(year)
      rental.rates.map do |rate|
        start_date = date_for_rate(rate["start"], year)
        end_date = date_for_rate(rate["end"], year)
        if end_date < start_date
          end_date = end_date.next_year
        end
        rate.merge("range" => start_date..end_date)
      end
    end

    def rate_for_date(date)
      range_date = Date.new(Date.today.year, date.month, date.day)
      comp_date = Date.today > range_date ? date.next_year : range_date
      rate_info = rates_with_range(range_date.year).select { |ri| ri["range"].include?(comp_date) }.first
      rate_info["rate"][/\d*$/].to_i
    end

    def cost_for_period(booking_range)
      #TODO: Move this out of rate calculator
      total = booking_range.inject(0) do |amount, date|
        amount + rate_for_date(date)
      end
      total
    end

    private

    def date_for_rate(rate_date, year)
      Date.new(year, rate_date[0..1].to_i, rate_date[3..4].to_i)
    end
  end
end
