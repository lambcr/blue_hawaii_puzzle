module BlueHawaii
  class Rental
    attr_reader :attributes

    def initialize(attributes)
      @attributes = attributes
    end

    def name
      @name ||= attributes["name"]
    end

    def seasonal_rates?
      @seasonal ||= attributes.has_key?("seasons")
    end

    def cleaning_fee
      @cleaning_fee ||= attributes.has_key?("cleaning fee") ? attributes["cleaning fee"][/\d*$/].to_i : 0
    end

    def sales_tax
      1.0411416
    end

    def rates
      if seasonal_rates?
        attributes["seasons"].map { |e| e.values.first }
      else
        [attributes.merge({"start" => "01-01", "end" => "12-31"})]
      end
    end

    def cost_for_period(start_date, end_date)
      date_range = start_date..end_date
      total = RateCalculator.new(self).cost_for_period(date_range)
      ((total + cleaning_fee) * sales_tax).round(2)
    end
  end
end
