require "json"

module BlueHawaii
  class RentalParser
    attr_reader :rentals_json

    def initialize(rentals_json)
      @rentals_json = rentals_json
    end

    def parse
      @all ||= parsed_list.map { |rental| Rental.new(rental) }
    end

    private

    def parsed_list
      @parsed_list ||= JSON.parse(rentals_json)
    end
  end
end
