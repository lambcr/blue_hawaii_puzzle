# BlueHawaii

A quick for fun solution to the [Blue Hawaii](http://www.puzzlenode.com/puzzles/7-blue-hawaii) puzzle.

## Installation

Add this line to your application's Gemfile:

    gem 'blue_hawaii', git: 'git@bitbucket.org:lambcr/blue_hawaii_puzzle.git'

And then execute:

    $ bundle

## Usage

```
require 'blue_hawaii'

json_path = File.join(File.dirname(__FILE__), "path_to_json.json")
text_path = File.join(File.dirname(__FILE__), "path_to_dates.txt")
costs = BlueHawaii.calculate_cost(json_path, text_path)
costs.each { |cost| puts cost }

```

## TODO

1) Better (any) error handling
2) Check file for valid formats
