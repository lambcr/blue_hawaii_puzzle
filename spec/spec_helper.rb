ENV["RAILS_ENV"] = 'test'

require 'rubygems'
require 'bundler/setup'
require 'rspec/autorun'

require 'blue_hawaii'
require 'date'

RSpec.configure do |config|
  config.mock_with :rspec
end
