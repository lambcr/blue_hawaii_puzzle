require 'spec_helper'

describe BlueHawaii::RateCalculator do
  let(:seasonal_rental) do
    BlueHawaii::Rental.new(
      {
        "name" => "Test Rental",
        "seasons" => [
          {"one" => {"start" => "02-01", "end" => "07-31", "rate" => "$165"}},
          {"two" => {"start" => "08-01", "end" => "01-31", "rate" => "$187"}}
        ]
      }
    )
  end

  describe "#rate_for_date" do
    let(:date) { Date.parse('2014-01-15') }
    let(:rate_for_date) { BlueHawaii::RateCalculator.new(seasonal_rental).rate_for_date(date) }

    it "returns an integer" do
      expect(rate_for_date).to be_kind_of(Integer)
    end
    it "returns correct rate or that date" do
      expect(rate_for_date).to eq(187)
    end
  end

  describe "#rates_with_range" do
    let(:date) { Date.parse('2014-01-15') }
    let(:rates_with_range) { BlueHawaii::RateCalculator.new(seasonal_rental).rates_with_range(date.year) }

    it "returns an array" do
      expect(rates_with_range).to be_instance_of(Array)
    end
    it "adds a range key to the rates" do
      rates_with_range.each do |rate_info|
        expect(rate_info).to have_key("range")
      end
    end
    it "creates a range value from the start and end dates" do
      august_start_rate = rates_with_range.select { |rate_info| rate_info["start"] == "08-01"}.first
      expect(august_start_rate["range"]).to eq(Date.parse("2014-08-01")..Date.parse("2015-01-31"))
    end
  end
end
