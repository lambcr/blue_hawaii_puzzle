require 'spec_helper'

describe BlueHawaii::VacationCostCalculator do

  let(:seasonal_rental) do
    BlueHawaii::Rental.new(
      {
        "name" => "Seasonal Rental",
        "cleaning fee" => "$42",
        "seasons" => [
          {"one" => {"start" => "02-01", "end" => "07-31", "rate" => "$165"}},
          {"two" => {"start" => "08-01", "end" => "01-31", "rate" => "$187"}}
        ]
      }
    )
  end
  let(:nonseasonal_rental) do
    BlueHawaii::Rental.new(
      {"name" => "Regular Rental", "rate" => "$250", "cleaning fee" => "$120"}
    )
  end
  let(:vacation_rentals) { [seasonal_rental, nonseasonal_rental] }

  subject { BlueHawaii::VacationCostCalculator.new(vacation_rentals, "2014/03/01 - 2014/03/08") }

  describe "#start_date" do
    it { expect(subject.start_date).to eq(Date.parse("2014-03-01")) }
  end

  describe "#end_date" do
    it "returns the last day minus one" do
      expect(subject.end_date).to eq(Date.parse("2014-03-07"))
    end
  end

  describe "#prices" do
    it { expect(subject.prices).to eq(["Seasonal Rental: $1246.25", "Regular Rental: $1946.93"]) }
  end
end
