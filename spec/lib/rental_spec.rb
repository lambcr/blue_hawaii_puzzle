require 'spec_helper'

describe BlueHawaii::Rental do

  let(:seasonal_rental) do
    BlueHawaii::Rental.new(
      {
        "name" => "Seasonal Rental",
        "seasons" => [
          {"one" => {"start" => "02-01", "end" => "07-31", "rate" => "$165"}},
          {"two" => {"start" => "08-01", "end" => "01-31", "rate" => "$187"}}
        ]
      }
    )
  end

  let(:nonseasonal_rental) do
    BlueHawaii::Rental.new(
      {"name" => "Test Rental", "rate" => "$250", "cleaning fee" => "$120"}
    )
  end

  describe "#rates" do
    context "for a seasonal rental" do
      let(:expected_rates) do
        [
          {"start" => "02-01", "end" => "07-31", "rate" => "$165"},
          {"start" => "08-01", "end" => "01-31", "rate" => "$187"}
        ]
      end

      it { expect(seasonal_rental.rates).to eq(expected_rates) }
    end

    context "for a non-seasonal rental" do
      let(:expected_rates) { [nonseasonal_rental.attributes.merge({"start" => "01-01", "end" => "12-31"})] }

      it { expect(nonseasonal_rental.rates).to eq(expected_rates) }
    end
  end

  describe "#seasonal_rates?" do
    context "for a seasonal rental" do
      it { expect(seasonal_rental.seasonal_rates?).to be_true }
    end

    context "for a non-seasonal rental" do
      it { expect(nonseasonal_rental.seasonal_rates?).to be_false }
    end
  end

  describe "#cost_for_period" do
    it "total up nightly cost then add cleaning fee and tax"  do
      expect(seasonal_rental.cost_for_period(Date.parse('2013-03-01'), Date.parse('2013-03-08'))).to eq(1374.31)
    end
  end

  describe "#name" do
    it { expect(seasonal_rental.name).to eq("Seasonal Rental") }
  end

  describe "#cleaning_fee" do
    it "returns 0 when cleaning fee is missing" do
      expect(seasonal_rental.cleaning_fee).to eq(0)
    end

    it "returns the cleaning fee as integer" do
      rental = BlueHawaii::Rental.new({"cleaning fee" => "$42"})
      expect(rental.cleaning_fee).to eq(42)
    end
  end
end
