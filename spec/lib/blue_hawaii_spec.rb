require 'spec_helper'

describe BlueHawaii do
  describe ".calculate_cost" do
    it "should work" do
      json_path = File.join(File.dirname(__FILE__), "../support/test_data/rental_rates.json")
      text_path = File.join(File.dirname(__FILE__), "../support/test_data/dates.txt")
      costs = BlueHawaii.calculate_cost(json_path, text_path)
      expected_output = ["Fern Grove Lodge: $2474.79", "Paradise Inn: $3508.65", "Honu's Hideaway: $2233.25"]
      expect(costs).to eq(expected_output)
    end
  end
end
